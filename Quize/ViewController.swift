//
//  ViewController.swift
//  Quize
//
//  Created by abdo emad  on 23/01/2024.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var nextQuestionLabelCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var currentQuestionLabelCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextQuestionLable: UILabel!
    @IBOutlet weak var currentQuestionLable: UILabel!
    @IBOutlet weak var answerLable: UILabel!
    var currentQuestionIndex : Int = 0
    
    var quize = [ Question(q: "what is 7+7?", a: "14"),
                  Question(q: "what is the capital of vermont?", a: "montplier"),
                  Question(q: "what is cognac made from?", a: "Grapes")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentQuestionLable.text = quize[currentQuestionIndex].question
        updateOffScreenLabel()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        nextQuestionLable.alpha = 0
    }
    func animateLabelTransitions() {
        view.layoutIfNeeded()
        let screenWidth = view.frame.width
        self.nextQuestionLabelCenterXConstraint.constant = 0
        self.currentQuestionLabelCenterXConstraint.constant += screenWidth
        
//        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, animations:{
//            self.currentQuestionLable.alpha = 0
//            self.nextQuestionLable.alpha = 1
//            self.view.layoutIfNeeded()
//        },completion: {_ in
//            swap(&self.currentQuestionLable, &self.nextQuestionLable)
//            swap(&self.currentQuestionLabelCenterXConstraint,
//                 &self.nextQuestionLabelCenterXConstraint)
//            self.updateOffScreenLabel()
//        }
//        )
        
        
                UIView.animate(withDuration: 0.5 , animations: {
                    self.currentQuestionLable.alpha = 0
                    self.nextQuestionLable.alpha = 1
                    self.view.layoutIfNeeded()
                }, completion: {_ in
                    swap(&self.currentQuestionLable, &self.nextQuestionLable)
                    swap(&self.currentQuestionLabelCenterXConstraint,&self   .nextQuestionLabelCenterXConstraint)
                    self.updateOffScreenLabel()
                })
    }
    
    func updateOffScreenLabel() {
        let screenWidth = view.frame.width
        nextQuestionLabelCenterXConstraint.constant = -screenWidth
        
    }
    
    
    @IBAction func showNextQuestion(_ sender: UIButton) {
        currentQuestionIndex += 1
        if currentQuestionIndex == quize.count {
            currentQuestionIndex = 0
        }
        nextQuestionLable.text = quize[currentQuestionIndex].question
        answerLable.text = "?????"
        animateLabelTransitions()
    }
    
    
    @IBAction func showAnswer(_ sender: UIButton) {
        answerLable.text = quize[currentQuestionIndex].answer
        
    }
    
}

struct Question {
    let question : String
    let answer : String
    
    init(q : String , a: String) {
        question = q
        answer = a
    }
}
